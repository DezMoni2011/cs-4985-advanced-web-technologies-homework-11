﻿using System;

/// <summary>
/// Code fiel for the Map webpage
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Spring 2015
/// </version>
public partial class Map : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}