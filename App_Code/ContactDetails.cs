﻿using System.Diagnostics;

/// <summary>
/// Class to keep track of a person's contact details
/// </summary>
/// <author>
/// CS4985
/// </author>
/// <version>
/// Spring 2015
/// </version>
public class ContactDetails
{
    private string firstName;
    private string lastName;
    private string email;
    private string phone;
    private string preferredContact;

    /// <summary>
    /// Gets or sets the first name.
    /// </summary>
    /// <value>
    /// The first name.
    /// </value>
    public string FirstName
    {
        get { return this.firstName; }
        set
        {
            Trace.Assert(value != null, "Invalid first name");
            this.firstName = value;
        }
    }

    /// <summary>
    /// Gets or sets the last name.
    /// </summary>
    /// <value>
    /// The last name.
    /// </value>
    public string LastName
    {
        get { return this.lastName; }
        set
        {
            Trace.Assert(value != null, "Invalid last name");
            this.lastName = value;
        }
    }

    /// <summary>
    /// Gets or sets the email address.
    /// </summary>
    /// <value>
    /// The email address.
    /// </value>
    public string Email
    {
        get { return this.email; }
        set
        {
            Trace.Assert(value != null, "Invalid email");
            this.email = value;
        }
    }

    /// <summary>
    /// Gets or sets the phone number.
    /// </summary>
    /// <value>
    /// The phone number.
    /// </value>
    public string Phone
    {
        get { return this.phone; }
        set
        {
            Trace.Assert(value != null, "Invalid phone number");
            this.phone = value;
        }
    }

    /// <summary>
    /// Gets or sets the preferred contact method.
    /// </summary>
    /// <value>
    /// The preferred contact method.
    /// </value>
    public string PreferredMethod
    {
        get { return this.preferredContact; }
        set
        {
            Trace.Assert(value != null, "Invalid contact method");
            this.preferredContact = value;
        }
    }
}